FROM debian:stretch

RUN set -ex \
    && mkdir -p /uploads /etc/apt/sources.list.d /var/cache/apt/archives/ \
    && export DEBIAN_FRONTEND=noninteractive \
    && apt-get clean \
    && apt-get update -y \
    && apt-get install -y \
        bash \
        openjdk-8-jre-headless \
        unzip \
        wget \
        python3-pip \
    && mkdir app

ENV VERSION 4.5
# RUN instead of ADD because Docker doesn't cache ADD layers and this doesn't need updating
RUN  wget https://www.languagetool.org/download/LanguageTool-$VERSION.zip

RUN unzip LanguageTool-$VERSION.zip \
    && rm LanguageTool-$VERSION.zip

RUN mkdir /nonexistent && touch /nonexistent/.languagetool.cfg

RUN pip3 install flask \
    gingerit \
    language_check \
    nltk \
    pywsd \
    textstat \
    vaderSentiment

RUN python3 -m nltk.downloader stopwords \
    averaged_perceptron_tagger \
    wordnet \
    punkt

RUN pip3 install spacy
RUN python3 -m spacy download en_core_web_lg

COPY . /app

RUN export LC_ALL=C.UTF-8 \
    && export LANG=C.UTF-8

EXPOSE 80
ENTRYPOINT ["python3"]
CMD ["app/app.py"]

# docker build -t simplifier .
# docker run -it --entrypoint "/bin/sh" simplifier 
# docker run -p 80:80 simplifier

