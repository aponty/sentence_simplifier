## To run:

#### To build Dockerfile: 
Docker container needs >= 3GB memory to run with spacy enabled; see
https://stackoverflow.com/questions/44533319/how-to-assign-more-memory-to-docker-container
to increase past default setting of 2GB
1. download/install/start docker
2. `cd` to directory containing dockerfile
3. run `docker build -t simplifier .` Note: this will be slow the first time, and subsequently fast. Docker caches each line in the Dockerfile separately (as a general rule)
4. `docker run -it --entrypoint "/bin/sh" simplifier` to get in a shell inside the container for dev/debugging/poking around
5. `docker run -p 80:80 simplifier` to run app inside container
6. Container is exposed to port 80. Try `curl -i -X POST -H 'Content-Type: application/json' -d '{"sentence":"Jackie traveled to the bank to deposit money once in a blue moon","target_grade": "2"}' http://localhost:80/`
7. standard `ctl + d` to exit

#### To run as a web server (outside of Docker):
1. cd to the directory where requirements.txt is located
2. activate your virtualenv
3. run: `pip install -r requirements.txt` and `python3 -m nltk.downloader all` in your shell
4. install LanguageTools and dependencies (see Dockerfile for what I used)
5. `FLASK_APP=app.py flask run`
    or 
    `FLASK_APP=app.py FLASK_ENV='development' flask run` to enable automatic reloading on file changes
    
    Localhost port 5000 is default

    http://flask.pocoo.org/ for documentation; access through any normal means

6.  try `curl -i -X POST -H 'Content-Type: application/json' -d '{"sentence":"Jackie traveled to the bank to deposit money once in a blue moon","target_grade": "2"}' http://localhost:5000/`

#### To run as a script: 
1. install dependencies (see above)
2. run `python3 main.py`. 
        This file is only used for testing; if you prefer testing through the web interface, feel free to delete it. I've included some examples of ways to dig into the classes.

## App API:

App has one route (POST) which accepts requests in this format (JSON): 
```
{ 
    "sentence": "original_sentence",
    "target_grade" : "int"
}
```

App response:
```
{
    "best_alternative": "Jackie travels to the bank to bank money once in a blue moon", 
    "synset_ids": [null, "travel.v.03", null, null, "bank.n.06", null, "deposit.v.02", "money.n.02", null], 
    "tokens": ["jackie", "traveled", "to", "the", "bank", "to", "deposit", "money", "once_in_a_blue_moon"],
    "definitions": [null, "make a trip for pleasure", null, null, "the funds held by a gambling house or the dealer in some gambling games", null, "put into a bank account", "wealth reckoned in terms of money", null], 
    "synonyms": [[], ["travel", "trip", "jaunt"], [], [], ["bank"], [], ["deposit", "bank", "stow"], ["money"], []], 
    "original_grade": 6.0, 
    "best_alternative_grade": 4.0
}
```
definitions and synonyms are on a per-token basis; excluded for common words, proper names, and idioms. All three keys (`tokens`, `definitions`, and `synonyms`) will have the same length and can be `zip()`'d or otherwise combined by index. 
## Adding Resources

#### Thesaurus

Unique keys in thesaurus (/resources/synonyms.py) are those used in WordNet. Example on how to find/select id for a given term is below. 

Without an ID, we would have to use similarity matching of definitions; while this is possible to do, and all necessary tools are already in the app, it would add another layer of error, as well as (likely) necessitate creating another set of ids/dictionary tree. WordNet ids are also commonly used by across python language tools.  

```
>>> from nltk.corpus import wordnet as wn

>>> my_new_word = 'bank'

>>> wn.synsets(my_new_word)                                                
[ Synset('bank.n.01'), Synset('depository_financial_institution.n.01'), Synset('bank.n.03'), Synset('bank.n.04'), Synset('bank.n.05'), Synset('bank.n.06'), Synset('bank.n.07'), Synset('savings_bank.n.02'), Synset('bank.n.09'), Synset('bank.n.10'), Synset('bank.v.01'), Synset('bank.v.02'), Synset('bank.v.03'), Synset('bank.v.04'), Synset('bank.v.05'), Synset('deposit.v.02'), Synset('bank.v.07')...

>>> wn.synset('bank.n.01').definition()                                   
'sloping land (especially the slope beside a body of water)'      

>>> wn.synset('bank.n.01').name()
'bank.n.01'
```
In short: 
1. Open shell -> `python3` -> `from nltk.corpus import wordnet as wn`
2. Find the key for the definition of the word you'd like to add synonyms for, and add it to the file. 
3. Multi-word synonmys should be joined with an underscore ('credit_union').

#### Proper Names and Idioms

Proper names can be added to `/resources/proper_names.py` as necessary. All tokens are downcased and excluded if present in the list; case and punctuation sensitive (keep it all lower case). Order does not matter.

Idioms can be added to `/resources/idioms.py`. Lower case and no punctuation, but untokenized (not joined with an underscore). Order does not matter.

`most_freq.py` is a sequential list of the most commonly used English words. This is used exclusively to pick out the most difficult words in a sentence (the alternative, going strictly off word length, seemed too crude). Adding/editing will not have any (significant) impact on overall results; I'd just let it be. 
