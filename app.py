from simplifier.sentence.sentence import Sentence
from flask import Flask
from flask import request
import os
import json as js
application = Flask(__name__)


@application.route('/', methods=['POST'])
def extract():
    data = js.loads(request.get_data().decode('utf-8'))

    orig_sen = data['sentence']
    target_grade = int(data['target_grade'])

    sen = Sentence(orig_sen, target_grade)
    res = {
        'best_alternative': sen.best_alternative(),
        'synset_ids': [tok.synset_id() for tok in sen.tokens],
        'tokens':  [tok.token for tok in sen.tokens],
        'definitions':  [tok.definition for tok in sen.tokens],
        'synonyms':  [tok.synonyms for tok in sen.tokens],
        'original_grade': sen.grade,
        'best_alternative_grade': sen.best_alternative_grade()
    }
    return js.dumps(res)

application.run(debug=False, host='0.0.0.0', port=80)
