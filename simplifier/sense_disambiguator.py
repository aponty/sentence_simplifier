from typing import List
from simplifier.sentence.token.token import Token
from pywsd import disambiguate
from pywsd.similarity import max_similarity as maxsim


class SenseDisambiguator:

    def set_synsets(self, tokens: List[Token]) -> List[Token]:
        sen = ' '.join([token.token for token in tokens])
        li_of_synsets = []
        # try/except needed as pywsd maxsim errors
        # if the NLTK part of speach != WordNet part of speach.
        # Could fork/fix/build package
        try:
            li_of_synsets.append(self.__maxsim_wup_synsets(sen))
            li_of_synsets.append(self.__maxsim_jcn_synsets(sen))
        except:
            li_of_synsets.append(self.__fallback(sen))

        for i, token in enumerate(tokens):
            for synsets in reversed(li_of_synsets):
                if synsets[i][1] is not None:
                    token.synset = synsets[i][1]
                    token.definition = token.synset.definition()
                    token.synonyms = [name for name in token.synset.lemma_names()
                                      if name is not token.token]
            token.extend_synonyms()

    def __maxsim_wup_synsets(self, sen: str) -> List[str]:
        return disambiguate(sen, algorithm=maxsim, similarity_option='wup')

    def __maxsim_jcn_synsets(self, sen: str) -> List[str]:
        return disambiguate(sen, algorithm=maxsim, similarity_option='jcn')

    def __fallback(self, sen: str) -> List[str]:
        return disambiguate(sen)
