from gingerit.gingerit import GingerIt  # api wrapper; cannot guarantee continued free access
import language_check
import textstat
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer
import spacy
print('loading spacy npl model')
nlp = spacy.load("en_core_web_lg")


class LangTools:
    @staticmethod
    def clean_string(string: str) -> str:
        # loss of connection/api limit could cause a range of errors
        try:
            string = GingerIt().parse(string + ' ')['result'].strip()
        except Exception as e:
            print(e)
        tool = language_check.LanguageTool('en-US')
        matches = tool.check(string)
        string = language_check.correct(string, matches)
        return string

    @staticmethod
    def grade_string(string: str) -> float:
        return textstat.text_standard(string, float_output=True)

    @staticmethod
    def sentiment(string: str) -> float:
        analyzer = SentimentIntensityAnalyzer()
        return analyzer.polarity_scores(string)['compound']

    @staticmethod
    def similarity(first: str, second: str) -> float:
        first = nlp(first)
        second = nlp(second)
        return first.similarity(second)
