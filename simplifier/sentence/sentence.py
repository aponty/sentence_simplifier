import nltk
import re
import textstat
import spacy
from spacy.tokenizer import Tokenizer
from spacy.lang.en import English
from typing import List
from simplifier.resources.idioms import idioms
from simplifier.resources.most_freq import most_freq
from simplifier.sentence.token.token import Token
from simplifier.sentence.alternative import Alternative
from simplifier.sense_disambiguator import SenseDisambiguator
from simplifier.lang_tools import LangTools
SWAPPABLE_WORDS_PER_SENTENCE = 10  # performance limit
nlp = English()
tokenizer = Tokenizer(nlp.vocab)


class Sentence:
    def __init__(self, sentence: str, target_grade: int) -> None:
        self.original_sentence = sentence
        self.target_grade = target_grade
        self.grade = LangTools.grade_string(self.original_sentence)
        self.sentiment = LangTools.sentiment(self.original_sentence)
        self.tokens = self.__tokenize(self.original_sentence)
        SenseDisambiguator().set_synsets(self.tokens)
        self.__set_token_scores()
        self.all_alternatives = self.__all_alternatives()

    def best_alternative(self) -> str:
        if self.__needs_reduction() and self.__has_alts():
            def by_grade(x): return abs(x.grade - self.target_grade)
            def by_sentiment(x): return abs(x.sentiment() - self.sentiment)
            def by_grade_and_sent(x): return by_grade(x) + by_sentiment(x)
            closest = min(self.all_alternatives, key=by_grade_and_sent)
            return closest.final()
        return self.original_sentence

    def best_alternative_grade(self) -> float:
        return LangTools.grade_string(self.best_alternative())

    # private

    def __needs_reduction(self) -> bool:
        return self.target_grade < self.grade

    def __has_alts(self) -> bool:
        return len(self.all_alternatives) > 1

    def __all_alternatives(self) -> None:
        alts = [self.tokens]
        for i, token in enumerate(self.tokens):
            if token.can_swap() and token.is_difficult:
                new_alts = []
                for synonym in token.synonyms:
                    for alt in alts:
                        new_alt = alt[:]
                        new_alt[i] = Token(token=synonym)
                        new_alts.append(new_alt)
                alts.extend(new_alts)
        all_permutations = [Alternative(tokens=alt, parent=self.original_sentence)
                            for alt in alts]
        return [alt for alt in all_permutations if alt.grade < self.grade]

    def __tokenize(self, sentence: str) -> List[Token]:
        # turns idioms -> tokens then words -> tokens
        sentence = self.__remove_punctuation(sentence.lower())
        present_idioms = [idiom for idiom in idioms if idiom in sentence]
        present_idioms = sorted(present_idioms, key=len, reverse=True)
        for idiom in present_idioms:
            sentence = sentence.replace(idiom, '_'.join(idiom.split(' ')))
        sentence = [token.text for token in tokenizer(sentence)]
        return [Token(sentence=sentence, token=token) for token in sentence]

    def __remove_punctuation(self, sentence: str) -> str:
        return re.sub(r'[^\w\s]', '', sentence)

    def __set_token_scores(self) -> None:
        word_frequency = []
        for i, token in enumerate(self.tokens):
            try:
                word_frequency.append(most_freq.index(self.tokens[i].token))
            except ValueError:
                word_frequency.append(10001)
        least_frequent = max([rank for rank in word_frequency])
        word_lengths = [len(token.token) for token in self.tokens]
        max_length = max(word_lengths)
        scores = [round(    0.5
                        *   (frequency/least_frequent)
                        +   0.5
                        *   (length/max_length)
                    , 5)
                  for frequency, length in zip(word_frequency, word_lengths)]
        priorities = sorted(scores, reverse=True)[:SWAPPABLE_WORDS_PER_SENTENCE]
        for i, token in enumerate(self.tokens):
            self.tokens[i].score = scores[i]
            if scores[i] in priorities:
                self.tokens[i].is_difficult = True
