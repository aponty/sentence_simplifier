from typing import List
import nltk
from nltk.corpus import stopwords
from simplifier.resources.proper_names import proper_names
from simplifier.resources.synonyms import synonyms


class Token:
    def __init__(self, token: str, sentence: List[str]=None) -> None:
        # synset, definition, synonyms set by SenseDisambiguator.set_synsets
        self.sentence = sentence
        self.token = token
        self.synset = None
        self.synonyms = []
        self.definition = None
        self.score = None
        self.is_difficult = None

    def synset_id(self) -> str:
        return self.synset.name() if self.synset else None

    def can_swap(self) -> bool:
        stoplist = set(stopwords.words('english'))
        not_proper = self.token not in proper_names
        not_stopword = self.token not in stoplist
        has_syns = len(self.synonyms) is not 0
        return not_proper and not_stopword and has_syns

    def extend_synonyms(self) -> None:
        if self.synset is not None:
            try:
                external_syns = synonyms[self.synset.name()]
                self.synonyms.extend(external_syns)
            except KeyError:
                pass
