from typing import List
from simplifier.sentence.token.token import Token
from simplifier.lang_tools import LangTools


class Alternative:
    def __init__(self, tokens: List[Token], parent: str) -> None:
        self.parent_sentence = parent
        self.tokens = tokens
        self.detokenized = self.__detokenize()
        self.grade = LangTools.grade_string(self.detokenized)

    # these public methods are very heavy; avoid/minimize batch calls
    def final(self) -> str:
        return LangTools.clean_string(self.detokenized)

    def sentiment(self) -> float:
        return LangTools.sentiment(self.detokenized)

    def similarity(self) -> float:
        return LangTools.similarity(self.detokenized, self.parent_sentence)

    # private

    def __detokenize(self) -> str:
        sentence = ' '.join([tok.token for tok in self.tokens])
        return sentence.replace('_', ' ')
