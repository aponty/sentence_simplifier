from simplifier.sentence.sentence import Sentence

# sen = 'Jack went to the bank to deposit money once in a blue moon'
# target_grade = 1
# sen = Sentence(sen, target_grade)
# result = sen.best_alternative()
# print(result)

sens = {
    2.0: 'Jack went to the bank to deposit money once in a blue moon',
    4.0: "For the record, Nate did not borrow his father's record player.",
    6.0: 'Queenie was in a fix because she had forgotten to fix the coffee machine on time.',
    1.0: 'She looked to her left after I left the room.',
    8.0: 'Form a straight line to collect your application form.',
    5.0: 'My mother objects to the objects bought at the flea market.',
    7.0: 'Ron could not find a safe enough place in their living room to place the crystal bowl.',
    9.0: 'Juliet sits looking at the sea waves while Bernie waves cheerfully at the children playing in the water.'
}
sen_objs = []

for grade in sens.keys():
    sen_objs.append(Sentence(sens[grade], grade-1))

print([[tok.definition for tok in sen.tokens] for sen in sen_objs])
print([[tok.synonyms for tok in sen.tokens] for sen in sen_objs])
print([sen.best_alternative() for sen in sen_objs])
